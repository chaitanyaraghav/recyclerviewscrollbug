package com.self.recyclerview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mListView;
    private Adapter mAdapter;
    private StaggeredGridLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mListView = (RecyclerView)findViewById(R.id.activity_main_test_recycler_view);
        mAdapter = new Adapter();

        mLayoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        mLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        mListView.setLayoutManager(mLayoutManager);

        mListView.setItemAnimator(new DefaultItemAnimator());
        mListView.setAdapter(mAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A basic recycler view adapter that has an item count of 1.
     * It draws a header that occupies the full span count.
     */
    public class Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.divider,viewGroup,false);
            return new DividerViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {

            final DividerViewHolder vh = (DividerViewHolder)viewHolder;

            if(vh!=null){

                StaggeredGridLayoutManager.LayoutParams itemParams =
                        (StaggeredGridLayoutManager.LayoutParams) vh.itemView.getLayoutParams();

                itemParams.setFullSpan(true); //make the divider full span

                vh.itemView.setLayoutParams(itemParams);

            }

        }

        @Override
        public int getItemCount() {
            return 1;
        }
    }

    /**
     * An empty view holder for the divider view
     */
    public class DividerViewHolder  extends RecyclerView.ViewHolder implements Serializable {

        public DividerViewHolder(View itemView) {
            super(itemView);

        }
    }
}
